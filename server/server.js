const express = require('express')
const cors = require('cors')
const app = express()
const apiPort = 8000

app.use(cors())

app.get('/', (req, res) => {
    res.send('Hello World!')
})

app.listen(apiPort, () => console.log(`Server running on port ${apiPort}`))