import React, { Component } from "react";
import "./Footer.css";

class Footer extends Component {
  render() {
    return (
      <footer className='row'>
        <div className='col col-sm-6'>
          <h2>Contact Us</h2>
          <form>
            <div className='form-group'>
              <label htmlFor='name'>Name</label>
              <input
                type='name'
                className='form-control'
                id='name'
                placeholder='Jane Doe'
              />
            </div>

            <div className='form-group'>
              <label htmlFor='email'>Email address</label>
              <input
                type='email'
                className='form-control'
                id='email'
                placeholder='name@example.com'
              />
            </div>
            <div className='form-group'>
              <label htmlFor='comments'>Your Comments</label>
              <textarea
                className='form-control'
                id='comments'
                rows='3'></textarea>
            </div>
            <button type='submit' className='btn btn-primary'>
              Submit
            </button>
          </form>
        </div>
      </footer>
    );
  }
}

export default Footer;