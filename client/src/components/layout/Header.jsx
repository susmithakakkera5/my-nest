// const Header = () => {
//     return (
//         <div>
            
//         </div>
//     )
// }

// export default Header


import React, { Component } from "react";
import Navbar from "./Navbar";
import "./Header.css";
import mynest from "../../images/mynest.jpg";

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLogged: true,
    };
    
  }
  // componentDidMount = () => {
  //   const token = localStorage.getItem("auth_jwt_token");
  //   const isLogged = token ? true : false;
  //   this.setState({ isLogged });
  //   console.log(this.state);
  // };
  render() {
    return (
      <div id='Header' className='row justify-content-md-end'>
        <div className='col col-sm-4'>
          <img id='logo' src={mynest} alt='nest' />
          <h1>
            <a href='/'>mynest</a>
          </h1>
        </div>
        <div className='col col-sm-8' hidden={this.state.isLogged}>
          <Navbar />
        </div>
      </div>
    );
  }
}

export default Header;