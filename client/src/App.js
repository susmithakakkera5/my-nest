import React from "react";
import Footer from "./components/layout/Footer";
// import { BrowserRouter as Router, Route } from "react-router-dom";
// import reactDom from "react-dom";
import Header from "./components/layout/Header";
import Login from "./components/auth/Login";
import Navbar from "./components/layout/Navbar";
// import SignIn from "./SignIn"



const App=() => {
  return (
    <div className="">
     <Header/>
     <Navbar/>
     <Login/>
     <Footer/>
     {/* <SignIn /> */}
    </div> 
  );
}
// class App extends React.Component{
//   render(){
//     return <h1>hello from class</h1>
//   }
// }
export default App;
