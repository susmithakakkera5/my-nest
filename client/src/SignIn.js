import React, { useState } from "react";
import TextField from "@material-ui/core/TextField";
import IconButton from "@material-ui/core/IconButton";
// import InputLabel from "@material-ui/core/InputLabel";
import Visibility from "@material-ui/icons/Visibility";
import InputAdornment from "@material-ui/core/InputAdornment";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
//import "./styles.css";

const SignIn = () => {
  const [Username, InputName] = useState("");
  const [values, setValues] = React.useState({
    password: "",
    showPassword: false,
  });
  
  const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword });
  };
  
  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };
  
  const handlePasswordChange = (prop) => (event) => {
    setValues({ ...values, [prop]: event.target.value });
  };


  return (
    <div
      style={{
        marginLeft: "40%"
      }}
    >
      <h2>Sign In</h2>
      <form>
        <TextField
          value={Username}
          label="Enter your Username"
          onChange={(e) => {
            InputName(e.target.value);
          }}
        />
      </form>
      <form>
        <TextField
          type={values.showPassword ? "text" : "password"}
          label="Enter your Password"
          onChange={handlePasswordChange("password")}
          value={values.password}
          endAdornment={
            <InputAdornment position="end">
              <IconButton
                onClick={handleClickShowPassword}
                onMouseDown={handleMouseDownPassword}
              >
                {values.showPassword ?
                <Visibility /> : <VisibilityOff />}
              </IconButton>
            </InputAdornment>
          }
        />
      </form>

      <form>
        <input id="keep signed in
        
        
        " type="checkbox" defaultChecked={true} />
        <label>Keep me signed in</label>
      </form>

      <form>
        <button type="button" className="sign_in">
          Sign In
        </button>

      </form>
      
    </div>
  );
};

export default SignIn;
